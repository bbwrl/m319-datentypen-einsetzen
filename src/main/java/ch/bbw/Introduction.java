package ch.bbw;

import java.util.Scanner;

public class Introduction {
    public static void main(String[] args) {

        // initialize from keyboard
        Scanner keyboard = new Scanner(System.in);

        // read String from keyboard
        String name = keyboard.nextLine();
        System.out.print("Name: ");
        System.out.println(name);

        // read number from keyboard
        int number = keyboard.nextInt();
        System.out.print("Nummer: ");
        System.out.println(number);

        // combined output
        System.out.println(String.format("Name ist %s und die Zahl ist %d", name, number));

        // or in two lines
        String output = String.format("Name ist %s und die Zahl ist %d", name, number);
        System.out.println(output);

        keyboard.close();
    }
}
