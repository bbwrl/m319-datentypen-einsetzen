package ch.bbw;

import java.util.Scanner;

public class SumMultipleInteger {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        int sum = 0;
        int number = 0;
        int count = 0;
        do {
            System.out.println("Please enter a number:");
            number = keyboard.nextInt();

            sum += number;
            count += number != 0 ? 1 : 0;

        } while(number != 0);

        System.out.println(
            String.format("The sum of all numbers (%d) is %d", count, sum));
        System.out.println(
            String.format("The average of all numbers is %.2f: ", (double)sum / (double)count));

        keyboard.close();
    }
}
