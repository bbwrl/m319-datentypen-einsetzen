package ch.bbw;

import java.util.Scanner;

public class Heartbeats {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please enter the number of heartbeats per minute:");
        int heartbeatsPerMinutes = keyboard.nextInt();

        System.out.println("Please enter an age in years:");
        int age = keyboard.nextInt();

        double minutesPerYear = 60 * 24 * 365.25;

        System.out.println("Heartbeats per minutes: " + heartbeatsPerMinutes);
        System.out.println("Age: " + age);
        System.out.print("Heartbeats over all years: ");
        System.out.println(String.format("%.0f", (double)minutesPerYear * (double)age * heartbeatsPerMinutes));
        System.out.println(minutesPerYear);

        keyboard.close();
    }
}
